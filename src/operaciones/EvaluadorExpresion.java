/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package operaciones;
import java.util.*;
import ufps.util.coleciones_seed.*;
/**
 *
 *  
 */
public class EvaluadorExpresion {
    String expresion;

/**
 *Constructor que recibe la expresion y la asigna
 *  
 */    
    public EvaluadorExpresion(String ex) {
        expresion=ex;
    }

/**
 *Constructor vacio
 *  
 */    
    public EvaluadorExpresion() {
    }

/**
 * Metodo que retorna la expresion cargada
 *  
 */    
    public String getExpresion() {
        return expresion;
    }

 /**
 *Metodo que modifica la expresion cargada
 *  
 */   
    public void setExpresion(String expresion) {
        this.expresion = expresion;
    }
    
 /**
 *Nos dice true si el char x es un simbolo de operacion matematica o false si no lo es
 *  
 */
    private boolean esSimbolo(char x){
        return x=='*' || x == '-'  || x=='+' || x== '/' || x== '^';
    }
    
 /**
 *True si el char x es un parentesis o corchete de cerrado, de lo cntrario devuelve false
 *  
 */
    private boolean esParentesisFin(char x){
        return x==')' || x==']';
    }
    
 /**
 * Dado un parentesis de cerrar obtiene su equivalente de abrir
 *  
 */    
    private char getParentesisContrario(char x){
        if(x==')')return '(';
        return '[';
    }
    
    
    /**
 * Dado un parentesis de abrir obtiene su equivalente de cerrar
 *  
 */    
    private char getParentesisContrarioCerrar(char x){
        if(x=='(')return ')';
        return ']';
    }
    
 /**
 *Metodo que nos indica si un char x es un digito
 *  
 */    
    private boolean esDigito(char x){
        return x>='0' && x<='9';
    }
    
/**
 *Metodo que verifica si la expresionmatematica  es correcta
 *  
 */    
    public boolean esCorrecta(){
        Pila<Character> p=new Pila<>();
        
        for (int i = 0; i < expresion.length(); i++) {
            if(esDigito(expresion.charAt(i))) continue;
            if(esParentesisFin(expresion.charAt(i)) && p.esVacia())return false;
            if(esParentesisFin(expresion.charAt(i))){
                if(esSimbolo(expresion.charAt(i-1)))return false;
                Character tmp=p.pop();
                if(tmp.equals(getParentesisContrario(expresion.charAt(i)))){
                    continue;
                }else{
                    p.push(tmp);
                    p.push(expresion.charAt(i));
                }
            }else{
                if(esSimbolo(expresion.charAt(i))){
                    if(i==0)return false;
                    boolean aux=esParentesisFin(expresion.charAt(i-1)) || esDigito(expresion.charAt(i-1));
                    if(esSimbolo(expresion.charAt(i-1)) || !aux)return false;
                    
                }else p.push(expresion.charAt(i));
            }
        }
       return p.esVacia();
    }
   
/**
 *Metodo que devuelve un String con la expresion matematica en notacion infija.
 *  
 */    
    public String getInfijo(){
        String in="";
        for (int i = 0; i < expresion.length()-1; i++) {
            in+=expresion.charAt(i);
            if(esDigito(expresion.charAt(i)) && esDigito(expresion.charAt(i+1))) continue;
            in+="  ";              
             }
        in+=expresion.charAt(expresion.length()-1);
        return in;
    }

/**
 *Dado un char x me devuelve un objeto de tipo Operador equivalente al char
 *  
 */    
    public Operador getOperador(char x) {
        String ops = "+-*/^";
        int i = ops.indexOf(x);
        if (i < 0) {
            return new Operador(x, i);
        }
        i /= 2;
        return new Operador(x, i);
    }
    
/**
 *metodo que a partir de la expresion en notacion infija, devuelve esa misma expresion pero en notacion postfija
 *  
 */    
    public String getPostfijo() {
        String po = "", in = getInfijo().replaceAll(" ", "");
        //System.out.println(in);
        Operador o = new Operador();
        Operador t = new Operador();
        Pila<Character> pila = new Pila<>();
        Character tmp='.';

        for (int i = 0; i < in.length() - 1; i++) {
            if (esDigito(in.charAt(i))) {
                po += in.charAt(i);
               // System.out.println("añadi a la rta: " + in.charAt(i));
                if (esDigito(in.charAt(i + 1))) {
                    continue;
                }
                po += "  ";
            } else {
                if (esParentesisFin(in.charAt(i))) {
                    char con = getParentesisContrario(in.charAt(i));
                   // System.out.println("con " + con);
                   // System.out.println("tope p " + pila.peek());
                   tmp=pila.pop();
                    while (con != tmp) {

                       // System.out.println("1 añadi a la rta: " + tmp);
                        po += tmp;
                        po += "  ";
                       // System.out.println("tope p " + tmp);
                       tmp=pila.pop();
                    }
                   // System.out.println(" borro p ");
                    

                } else {
                    if (!esSimbolo(in.charAt(i))) {
                        pila.push(in.charAt(i));

                        //System.out.println("2 añadi a la pila: " + pila.peek());
                    } else {
                        if (pila.esVacia()) {
                            pila.push(in.charAt(i));
                           // System.out.println("añadi a la pila: " + in.charAt(i));
                        } else {
                            o = getOperador(in.charAt(i));
                            tmp=pila.pop();
                            t = getOperador(tmp);
                            pila.push(tmp);

                            while (o.esMenor(t) && pila.getTamano()>1) {
                                //System.out.println("añadi a la rta: " + tmp);
                                po += pila.pop();
                                po += "  ";
                                tmp=pila.pop();
                                t = getOperador(tmp);
                                pila.push(tmp);
                            }
                            if (o.esMayor(t)) {
                                pila.push(in.charAt(i));
                               // System.out.println("añadi a la pila: " + in.charAt(i));
                            } else {
                               // System.out.println("3 añadi a la rta: " + pila.peek());
                               // System.out.println("priT " + getOperador(pila.peek()).prioridad);
                                po += pila.pop();
                                po += "  ";
                                pila.push(in.charAt(i));
                                //System.out.println("añadi a la pila: " + in.charAt(i));
                            }
                        }
                    }
                }
            }

        }
        //System.out.println("ya");
        char ul = in.charAt(in.length() - 1);
        if (esDigito(ul)) {
            po += ul + "  ";
            while (!pila.esVacia()) {
                po += pila.pop();
                if (!pila.esVacia()) {
                    po += "  ";
                }
            }
        } else {
            while (!pila.esVacia()) {
                tmp=pila.pop();
                pila.push(tmp);
                //System.out.println("mp: "+tmp);
                if (esSimbolo(tmp)) {
                    po += pila.pop();
                    if(pila.esVacia())continue;
                    tmp=pila.pop();
                    pila.push(tmp);
                    boolean tam=pila.getTamano()>1;
                     boolean tam2=pila.getTamano()==1;
                     //System.out.println(tmp+" t "+pila.getTamano());
                      boolean sim=esSimbolo(tmp);
                    if (tam ||( tam2 && sim )) {
                        po += "  ";
                    }
                }else pila.pop();
            }
        }
        //System.out.println(po);
        return po;
    }
    
    
/**
 *metodo que a partir de la expresion en notacion infija, devuelve esa misma expresion pero en notacion prefija
 *  
 */    
    public String getPrefijo() {
        String po = "", in = getInfijo().replaceAll(" ", "");
        //System.out.println(in);
        Operador o = new Operador();
        Operador t = new Operador();
        Pila<Character> pila = new Pila<>();

        for (int i = in.length() - 1; i >0 ; i--) {
            if (esDigito(in.charAt(i))) {
                po += in.charAt(i);
               // System.out.println("añadi a la rta: " + in.charAt(i));
                if (esDigito(in.charAt(i - 1))) {
                    continue;
                }
                po += "  ";
            } else {
                if (esParentesisFin(in.charAt(i))) {
                    pila.push(in.charAt(i));

                } else {
                    if (!esSimbolo(in.charAt(i))) {                     
                       
                         char con = getParentesisContrarioCerrar(in.charAt(i));
                   // System.out.println("con " + con);
                   // System.out.println("tope p " + tmp);
                   Character tmp=pila.pop();
                    while (con != tmp) {
                       
                       // System.out.println("1 añadi a la rta: " + tmp));
                        po += tmp;
                        po += "  ";
                        tmp=pila.pop();
                       // System.out.println("tope p " + tmp);
                    }
                   // System.out.println(" borro p ");
                    
                        
                        //System.out.println("2 añadi a la pila: " + tmp);
                    } else {
                        if (pila.esVacia()) {
                            pila.push(in.charAt(i));
                           // System.out.println("añadi a la pila: " + in.charAt(i));
                        } else {
                            o = getOperador(in.charAt(i));
                            Character tmp=pila.pop();
                            t = getOperador(tmp);
                            pila.push(tmp);

                            while (o.esMenor(t) && pila.getTamano()>1) {
                                //System.out.println("añadi a la rta: " + tmp);
                                po += pila.pop();
                                po += "  ";
                                tmp=pila.pop();
                                t = getOperador(tmp);
                                pila.push(tmp);
                            }
                            if (o.esMayor(t)) {
                                pila.push(in.charAt(i));
                               // System.out.println("añadi a la pila: " + in.charAt(i));
                            } else {
                               // System.out.println("3 añadi a la rta: " + tmp);
                               // System.out.println("priT " + getOperador(tmp).prioridad);
                                po += pila.pop();
                                po += "  ";
                                pila.push(in.charAt(i));
                                //System.out.println("añadi a la pila: " + in.charAt(i));
                            }
                        }
                    }
                }
            }

        }
        //System.out.println("ya");
        char ul = in.charAt(0);
        if (esDigito(ul)) {
           // System.out.println("dig");
            po += ul + "  ";
            while (!pila.esVacia()) {
                po += pila.pop();
                if (!pila.esVacia()) {
                    po += "  ";
                }
            }
        } else {
            //System.out.println("no dig");
            while (!pila.esVacia()) {
                Character tmp=pila.pop();
                pila.push(tmp);
                if (esSimbolo(tmp)) {
                    po += tmp;
                    
                    tmp=pila.pop();
                    pila.push(tmp);
                    if (pila.getTamano()>1 ||(pila.getTamano()==1 && esSimbolo(tmp))) {
                        po += "  ";                        
                    }
                   
                }
            }
        }
       // System.out.println(po);
        StringBuilder tmp = new StringBuilder();
        tmp.append(po);
        po=tmp.reverse().toString();
        return po;
    }
    
    
    
/**
 *Realiza la operacion indicada por el char c, entre los long "x" y "y"
 *  
 */    
    public float getOperacion(char c,float x, float y){
        float rta=0;
        switch(c){
            case '+': 
                rta=y+x;
                break;
            case '-': 
                rta=y-x;
                break;
            case '*': 
                rta=y*x;
                break;
            case '/': 
                rta=y/x;
                break;
            case '^': 
                rta=(int) Math.pow(y, x);
                break;
            default: break;
        }
        return rta;
    }
    
/**
 *Metodo que a partir de una expresion en postfijo retorna un valor float con el resultado de la expresion
 *  
 */    
    public float getEvaluarExpresion(){
        float rta=0;
        String[] postFijo=getPostfijo().split("  ");
        Pila<Float> pila=new Pila<>();
        float x=0,y=0;
        char o=' ';
        for (int i = 0; i < postFijo.length; i++) {
            o=postFijo[i].charAt(0);
          if(esSimbolo(o)){
              x=pila.pop();
              y=pila.pop();
              pila.push(getOperacion(o, x, y));              
          }else{
              pila.push(Float.parseFloat(postFijo[i]));
          } 
        }
        if(pila.getTamano()!=1)System.out.println("ERROR");
        else rta=pila.pop();
        
        return rta;
    }
    
}
