/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operaciones;

/**
 *
 * 
 */
public class Operador {
    char valor;
    int prioridad;

/**
 *Constructor vacio
 *  
 */    
    public Operador() {
    }

/**
 *constructor con parametros
 *  
 */    
    public Operador(char valor, int prioridad) {
        this.valor = valor;
        this.prioridad = prioridad;
    }
/**
 *Indica si this es un operador con mayor prioridad que un operador o
 *  
 */
    public boolean esMayor(Operador o){
        return this.prioridad>o.prioridad;
    }
    
/**
 *Indica si this es un operador con menor prioridad que un operador o
 *  
 */    
    public boolean esMenor(Operador o){
        return this.prioridad<o.prioridad;
    }
 
/**
 * Metodo que retorna el char con valor del operador
 *  
 */    
    public char getValor() {
        return valor;
    }

    
/**
 *Metodo que modifica el valor del operador
 *  
 */    
    public void setValor(char valor) {
        this.valor = valor;
    }

/**
 *Metodo que retorna la prioridad del operador
 * 
 */   
    public int getPrioridad() {
        return prioridad;
    }

/**
 *Metodo que modifica la prioridad del operador
 *  
 */     
    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }
    
    
}
